//
//  SharedFunction.swift
//  assessment
//
//  Created by sookmun on 04/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class SharedFunction {
    
    
    static func showAlert(on vc:UIViewController,with title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async {
            vc.present(alert, animated: true, completion: nil)
        }
    }
    
    static func convertCurrencyToDouble(input: String) -> Double? {
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .currency
        numberFormatter.locale = Locale(identifier: "ms_MY")
        return numberFormatter.number(from: input)?.doubleValue
    }
    
}
