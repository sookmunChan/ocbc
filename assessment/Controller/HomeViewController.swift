//
//  HomeViewController.swift
//  assessment
//
//  Created by sookmun on 04/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import UIKit
import CoreData
import CurrencyTextField

class HomeViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var topupTxtField: CurrencyTextField!
    @IBOutlet weak var topupView: UIView!
    @IBOutlet weak var profileLbl: UILabel!
    @IBOutlet weak var balanceLbl: UILabel!
    
    var username = ""
    var userId = 0
    var balance = 0.0
    var currentString = ""
    var oweAmount = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.username =  UserDefaults.standard.string(forKey: "username") ?? ""
        self.userId = UserDefaults.standard.integer(forKey: "id")
        print("userid::\(self.userId)")
        profileLbl.text = "\(username)'s Profile"
        
        self.topupTxtField.delegate = self
        
        getData()
    }
    
    func getData(){
        
        self.balance = 0.0
        
        print("get data")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "id == %d", self.userId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                self.balance = data.value(forKey: "balance") as! Double
            };DispatchQueue.main.async {
                
                print("updated")
                self.balanceLbl.text = String(format: "Balance: RM %.2f", self.balance)
            }
            
        } catch { //error getting password
            
            print("Failed")
            SharedFunction.showAlert(on: self, with: "", message: "User is not found")
            return
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.topupTxtField.resignFirstResponder()
    }
    
    //textfield delegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
    
    @IBAction func topupBtnClicked(_ sender: UIButton) {
        
        self.topupView.isHidden = false
    }
    
    @IBAction func debtBtnClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "showDebt", sender: self)
    }
    
    @IBAction func transferBtnClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "showTransfer", sender: self)
    }
    
    @IBAction func transactionHistoryBtnClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "showTransactionHistory", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "showTransfer"){
            let vc = self.storyboard?.instantiateViewController(identifier: "TransferViewController") as! TransferViewController
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    //topupview handle
    @IBAction func confirmTopupBtnClicked(_ sender: UIButton) {
        
        let amount = self.topupTxtField.text ?? ""
        print("amount::\(amount)")
        
        if (amount == "" || amount == "RM0.00"){
            
            SharedFunction.showAlert(on: self, with: "", message: "Required field is left empty")
            return
        }
        
        let doubleValue = SharedFunction.convertCurrencyToDouble(input: amount)!
        let totalAmount = doubleValue + self.balance
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "id == %d", self.userId)
        request.returnsObjectsAsFaults = false
        
        do{
            let results = try context.fetch(request) as? [NSManagedObject]
            if results?.count != 0 {
                
                results?[0].setValue(totalAmount, forKey: "balance")
            }
            
        }catch{
            
            print("error executing fetch request: \(error)")
        }
        
        do{
            try context.save()
            let alert = UIAlertController(title: "", message: "Successfully topup.", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
                self.topupTxtField.resignFirstResponder()
                self.topupTxtField.text = ""
                self.topupView.isHidden = true
                self.viewDidLoad()
            }))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
        }catch{
            print("error executing fetch request: \(error)")
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: UIButton) {
        
        self.topupTxtField.resignFirstResponder()
        self.topupTxtField.text = ""
        self.topupView.isHidden = true
    }
    
    @IBAction func unwindToHomeViewFromTransferView(segue:UIStoryboardSegue) {
        
        getData()
    }
    
    @IBAction func unwindFromTransactionHistoryToHomeView(segue:UIStoryboardSegue) {
        
        getData()
    }
    
    @IBAction func unwindFromDebtRecordsToHomeView(segue:UIStoryboardSegue) {
        
        getData()
    }
    
    //logout
    @IBAction func logoutBtnClicked(_ sender: UIButton) {
        
        let appDomain = Bundle.main.bundleIdentifier
        UserDefaults.standard.removePersistentDomain(forName: appDomain!)
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        vc.modalPresentationStyle = .fullScreen
        self.present(vc, animated: true, completion: nil)
    }
}
