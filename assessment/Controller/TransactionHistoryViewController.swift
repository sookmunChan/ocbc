//
//  TransactionHistoryViewController.swift
//  assessment
//
//  Created by sookmun on 07/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import UIKit
import CoreData

class TransactionHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var userId = 0
    var nameArr = [String]()
    var dateArr = [Date]()
    var currencyArr = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.userId = UserDefaults.standard.integer(forKey: "id")
        getData()
    }
    
    
    func getData(){
        
        print("get data")
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        request.predicate = NSPredicate(format: "from_user_id == %d", self.userId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)

            for data in result as! [NSManagedObject] {
                
                self.currencyArr.append(data.value(forKey: "amount") as! Double)
                self.dateArr.append(data.value(forKey: "create_date") as! Date)
                self.nameArr.append(data.value(forKey: "target_name") as! String)
                
            };DispatchQueue.main.async {
                
                print("updated")
                self.tableView.reloadData()
                
            }
            
        } catch { //error getting password
            
            print("Failed")
            SharedFunction.showAlert(on: self, with: "", message: "User is not found")
            return
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 93.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.currencyArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "TransactionHistoryTableViewCell") as! TransactionHistoryTableViewCell
        
        cell.nameLbl.text = self.nameArr[indexPath.row]
        
        if (self.currencyArr[indexPath.row] < 0){
            
            var str = String(format: "%.2f", self.currencyArr[indexPath.row])
            str.insert("R", at: str.index(str.startIndex, offsetBy: 1))
            str.insert("M", at: str.index(str.startIndex, offsetBy: 2))
            cell.currencyLbl.text = str
        }else{
            cell.currencyLbl.text = String(format: "RM%.2f", self.currencyArr[indexPath.row])
        }
        
        cell.dateLbl.text = "\(self.dateArr[indexPath.row])"
        
        return cell
    }

    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "unwindFromTransactionHistoryToHomeViewWithSegue", sender: self)
    }
    
}
