//
//  LoginViewController.swift
//  assessment
//
//  Created by sookmun on 04/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import UIKit
import CoreData

class LoginViewController: UIViewController {
    
    @IBOutlet weak var usernameTxtField: CustomOutlinedTxtField!
    @IBOutlet weak var passwordTxtField: CustomHidePwdTxtField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        let username =  UserDefaults.standard.string(forKey: "username") ?? ""
        print("username \(username)")
        if (username != ""){
            
            self.performSegue(withIdentifier: "showHome", sender: self)
            
        }
    }
    
    @IBAction func signInBtnClicked(_ sender: UIButton) {
        
        let username = usernameTxtField.textField.text ?? ""
        let password = passwordTxtField.textField.text ?? ""
        
        if (username == "" || password == ""){
            
            SharedFunction.showAlert(on: self, with: "", message: "Required field is left empty")
            return
        }
        
        //get user password
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchRequest.includesSubentities = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        fetchRequest.predicate = NSPredicate(format: "username == %@", username)
        
        do {
            
            let result = try context.fetch(fetchRequest)
            print("result::\(result)")
            
            if (result.isEmpty == true){
                
                SharedFunction.showAlert(on: self, with: "", message: "User is not found")
                return
            }
            
            for data in result as! [NSManagedObject] {
                
                print("data:\(data)")
                let savedPwd = data.value(forKey: "password") as! String
                let userId = data.value(forKey: "id") as! Int
                if (password == savedPwd){
                    
                    UserDefaults.standard.set(username, forKey: "username")
                    UserDefaults.standard.set(userId, forKey: "id")
                    
                    let vc = self.storyboard?.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
                    self.present(vc, animated: true, completion: nil)
                    
                }else{
                    SharedFunction.showAlert(on: self, with: "", message: "User or Password is incorrect")
                    return
                }
            }
            
        } catch { //error getting password
            
            print("Failed")
            SharedFunction.showAlert(on: self, with: "", message: "User is not found")
            return
        }
        
    }
    
}
