//
//  SignUpViewController.swift
//  assessment
//
//  Created by sookmun on 04/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import UIKit
import CoreData

class SignUpViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var usernameTxtField: CustomOutlinedTxtField!
    @IBOutlet weak var passwordTxtField: CustomHidePwdTxtField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    
    @IBAction func signUpBtnClicked(_ sender: UIButton) {
        
        let username = usernameTxtField.textField.text ?? ""
        let password = passwordTxtField.textField.text ?? ""
        
        print("username:\(username) \(password)")

        if (username == "" || password == "") {

            SharedFunction.showAlert(on: self, with: "", message: "Required field is left empty")
            
            return
        }

        sameEntityExists(username: username, password: password)

    }
    
    func sameEntityExists(username: String, password: String){ //check for exisitng username
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchRequest.includesSubentities = false
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext

        var entitiesCount = 0
        do {
            entitiesCount = try context.count(for: fetchRequest)
            let userId = entitiesCount + 1
            print("entitiesCount:: \(entitiesCount) \(userId)")
            
            if (entitiesCount > 0){
            
                fetchRequest.predicate = NSPredicate(format: "username CONTAINS[c] %@", username)
                let result = try context.fetch(fetchRequest)
                print("result::\(result)")
                
                if (result.isEmpty == true){

                    addUser(context: context, username: username, password: password, id: userId)
                }else{

                    self.usernameTxtField.value = ""
                    self.passwordTxtField.value = ""
                    SharedFunction.showAlert(on: self, with: "", message: "Username is exists")
                }
                
            }else{
                
                addUser(context: context, username: username, password: password, id: userId)
            }
            
        }
        catch {
            print("error executing fetch request: \(error)")
        }

    }
    
    func addUser(context: NSManagedObjectContext, username: String, password: String, id: Int){
        
        let entity = NSEntityDescription.entity(forEntityName: "Users", in: context)
        let newUser = NSManagedObject(entity: entity!, insertInto: context)
        newUser.setValue(username, forKey: "username")
        newUser.setValue(password, forKey: "password")
        newUser.setValue(id, forKey: "id")
        
        do {
           try context.save()
            let alert = UIAlertController(title: "", message: "Successfully registered", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
                self.dismiss(animated: true, completion: nil)
            }))
            DispatchQueue.main.async {
                self.present(alert, animated: true, completion: nil)
            }
          } catch {
           print("Failed saving")
            SharedFunction.showAlert(on: self, with: "", message: "Failed to register")
        }
    }
}
