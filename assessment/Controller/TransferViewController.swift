//
//  TransferViewController.swift
//  assessment
//
//  Created by sookmun on 07/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import UIKit
import DropDown
import CurrencyTextField
import CoreData
import CurrencyText

class TransferViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var amountTxtField: UITextField!
    @IBOutlet weak var dropDownBtn: UIButton!
    private var textFieldDelegate: CurrencyUITextFieldDelegate!
    let dropDown = DropDown()
    
    var idArray = [Int]()
    var usernameArray = [String]()
    var balanceArray = [Double]()
    var userId = 0
    var toUser = 0
    var accBalance = 0.0
    var targetName = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.userId = UserDefaults.standard.integer(forKey: "id")
        amountTxtField.delegate = self
        getUserLists()
        getAccBalance()
        setupTextFieldWithCurrencyDelegate()
        
    }
    
    func getAccBalance(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        request.predicate = NSPredicate(format: "id == %d", self.userId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                self.accBalance = data.value(forKey: "balance") as! Double
            }
            
        } catch { //error getting password
            
            print("Failed")
            SharedFunction.showAlert(on: self, with: "", message: "User is not found")
            return
        }
    }
    
    func getUserLists(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                
                let id = data.value(forKey: "id") as! Int
                
                if (id != self.userId){
                    
                    self.idArray.append(id)
                    self.usernameArray.append(data.value(forKey: "username") as! String)
                    self.balanceArray.append(data.value(forKey: "balance") as! Double)
                }
                
            };DispatchQueue.main.async {
                
                self.dropDown.anchorView = self.dropDownBtn
                self.dropDown.bottomOffset = CGPoint(x: 0, y: self.dropDownBtn.bounds.height)
                self.dropDown.dataSource = self.usernameArray
            }
            
        } catch { //error getting password
            
            print("Failed")
            SharedFunction.showAlert(on: self, with: "", message: "User is not found")
            return
        }
    }
    
    private func setupTextFieldWithCurrencyDelegate() {
        let currencyFormatter = CurrencyFormatter {
            $0.minValue = 0
            $0.currency = .malaysianRinggit
            $0.locale = CurrencyLocale.malayMalaysia
            $0.hasDecimals = true
        }
        
        textFieldDelegate = CurrencyUITextFieldDelegate(formatter: currencyFormatter)
        textFieldDelegate.clearsWhenValueIsZero = true
        textFieldDelegate.passthroughDelegate = self
        
        amountTxtField.delegate = textFieldDelegate
        amountTxtField.keyboardType = .numberPad
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.amountTxtField.resignFirstResponder()
    }
    
    @IBAction func userListBtnClicked(_ sender: UIButton) {
        
        self.dropDown.show()
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dropDownBtn.setTitle(item, for: .normal)
            self.toUser = self.idArray[index]
            self.targetName = self.usernameArray[index]
        }
    }
    
    @IBAction func confirmBtnClicked(_ sender: UIButton) {
        
        let receipient = self.dropDownBtn.titleLabel?.text
        let amount = self.amountTxtField.text ?? ""
        
        if (receipient == "Please select who to transfer" || amount == ""){
            
            SharedFunction.showAlert(on: self, with: "", message: "Required field is left empty")
            return
        }
        
        let doubleValue = SharedFunction.convertCurrencyToDouble(input: amount)!
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let context = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Users")
        fetchRequest.includesSubentities = false
        fetchRequest.predicate = NSPredicate(format: "id == %d", self.userId)
        let updatedBalance = self.accBalance - doubleValue
        
        do{

            let results = try context.fetch(fetchRequest) as? [NSManagedObject]

            if results?.count != 0 { // Atleast one was returned

                if (updatedBalance < 0){
                    results?[0].setValue(0, forKey: "balance")

                    addDebt(context: context, doubleValue: updatedBalance, date: date)
                    addTransaction(context: context, doubleValue: updatedBalance, date: date)
                }else{
                    
                    results?[0].setValue(updatedBalance, forKey: "balance")
                    addTransaction(context: context, doubleValue: doubleValue, date: date)
                }

            }

            do{
                try context.save()
                let alert = UIAlertController(title: "", message: "Successfully transferred", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
                    self.performSegue(withIdentifier: "unwindToHomeViewFromTransferViewWithSegue", sender: self)
                }))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            }catch{
                print("error executing fetch request: \(error)")
            }

        }catch{
            print("error executing fetch request: \(error)")
        }
    }
    
    
    func addDebt(context: NSManagedObjectContext, doubleValue: Double, date: Date){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Owing")
        fetchRequest.includesSubentities = false

        var owningCount = 0
        do {
            owningCount = try context.count(for: fetchRequest)
            let newId = owningCount + 1
            let entity = NSEntityDescription.entity(forEntityName: "Owing", in: context)
            let newOwing = NSManagedObject(entity: entity!, insertInto: context)
            newOwing.setValue(doubleValue, forKey: "amount")
            newOwing.setValue(self.userId, forKey: "from_id")
            newOwing.setValue(self.toUser, forKey: "target_id")
            newOwing.setValue(date, forKey: "create_date")
            newOwing.setValue(newId, forKey: "id")
            newOwing.setValue(self.targetName, forKey: "target_name")
        }catch{
            print("error executing fetch request: \(error)")
        }
    }
    
    func addTransaction(context: NSManagedObjectContext, doubleValue: Double, date: Date){
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Transaction")
        fetchRequest.includesSubentities = false

        var entitiesCount = 0
        do {
            entitiesCount = try context.count(for: fetchRequest)
            let newId = entitiesCount + 1
            let entity = NSEntityDescription.entity(forEntityName: "Transaction", in: context)
            let newTransaction = NSManagedObject(entity: entity!, insertInto: context)
            newTransaction.setValue(doubleValue, forKey: "amount")
            newTransaction.setValue(self.userId, forKey: "from_user_id")
            newTransaction.setValue(self.toUser, forKey: "to_user_id")
            newTransaction.setValue(date, forKey: "create_date")
            newTransaction.setValue(newId, forKey: "id")
            newTransaction.setValue(self.targetName, forKey: "target_name")
            
        }catch{
            print("error executing fetch request: \(error)")
        }
    }
    
    @IBAction func backBtnClicked(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "unwindToHomeViewFromTransferViewWithSegue", sender: self)
    }
    
}
