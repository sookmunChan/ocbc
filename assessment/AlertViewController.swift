//
//  AlertViewController.swift
//  assessment
//
//  Created by sookmun on 05/06/2020.
//  Copyright © 2020 OCBC. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {
    
    static func showAlert(on vc:UIViewController,with title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: {action in
            self.dismiss(self)
        }))
        UIApplication.shared.delegate?.window??.rootViewController?.present(alert, animated: true)
    }

}
